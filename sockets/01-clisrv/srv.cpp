#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

using namespace std;

int main()
{
	struct sockaddr_in local;
	int s, s1, s2;
	int rc;
	char buf[16];

	local.sin_family = AF_INET;
	local.sin_port = htons(7500);
	local.sin_addr.s_addr = htonl(INADDR_ANY);

	if ( (s = socket(AF_INET,SOCK_STREAM,0)) < 0 )
	{
		cerr << "Error create socket\n";
		return 1;
	}

	if ( bind(s,(struct sockaddr *)&local,sizeof(local)) < 0 )
	{
		cerr << "Unable bind socket\n";
		return 2;
	}

	if ( listen(s,5) )
	{
		cerr << "Unable listen socket\n";
		return 3;
	}

	if ( (s1 = accept(s,NULL,NULL)) < 0 )
	{
		cerr << "Unable accept connection\n";
		return 4;
	}
	else
	{
		struct sockaddr_in s1_addr;
		socklen_t socklen = sizeof(s1_addr);
		if ( !getpeername(s1,(sockaddr *)&s1_addr,&socklen) )
			cout << "Connected client " << inet_ntoa(s1_addr.sin_addr) << ":" << (unsigned short)s1_addr.sin_port << endl;
	}

	if ( (s2 = accept(s,NULL,NULL)) < 0 )
	{
		cerr << "Unable accept connection\n";
		return 5;
	}
	else
	{
		struct sockaddr_in s2_addr;
		socklen_t socklen = sizeof(s2_addr);
		if ( !getpeername(s2,(sockaddr *)&s2_addr,&socklen) )
			cout << "Connected client " << inet_ntoa(s2_addr.sin_addr) << ":" << (unsigned short)s2_addr.sin_port << endl;
	}

	if ( fork() )
	{
		while( 1 )
		{
			if ( recv(s1,buf,1,0) > 0 )
				send(s2,buf,1,0);
		}
	}

	if ( fork() )
	{
		while( 1 )
		{
			if ( recv(s2,buf,1,0) > 0 )
				send(s1,buf,1,0);
		}
	}

	while( 1 )
		sleep(1);

	shutdown(s,2);
	shutdown(s1,2);
	shutdown(s2,2);

	close(s);
	close(s1);
	close(s2);

	return 0;
}
