#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string>

using namespace std;

int main()
{
	struct sockaddr_in peer;
	int s;
	int rc;
	char buf[129];

	peer.sin_family = AF_INET;
	peer.sin_port = htons(7500);
	peer.sin_addr.s_addr = inet_addr("127.0.0.1");

	if ( (s = socket(AF_INET,SOCK_STREAM,0)) < 0 )
	{
		cerr << "Error create socket\n";
		return 1;
	}

	if ( connect(s,(struct sockaddr *)&peer,sizeof(peer)) )
	{
		cerr << "Unable connect\n";
		return 2;
	}

	if ( fork() )
	{
		while( 1 )
		{
			ssize_t n;
			if ( (n = recv(s,buf,128,0)) > 0 )
			{
				buf[n] = '\0';
				cout << string(buf) << endl;
			}
		}
	}

	string str;
	while( !cin.eof() )
	{
		getline(cin,str);
		send(s,str.c_str(),str.length(),0);
	}

	while ( 1 )
	{
		sleep(1);
	}

	shutdown(s,2);
	close(s);

	return 0;
}
